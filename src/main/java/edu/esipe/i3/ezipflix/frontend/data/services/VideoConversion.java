package edu.esipe.i3.ezipflix.frontend.data.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.api.client.util.Lists;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.PubsubMessage;
import com.amazonaws.*;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

import edu.esipe.i3.ezipflix.frontend.ConversionRequest;
import edu.esipe.i3.ezipflix.frontend.ConversionResponse;
import edu.esipe.i3.ezipflix.frontend.data.entities.VideoConversions;
import edu.esipe.i3.ezipflix.frontend.data.repositories.VideoConversionRepository;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by Gilles GIRAUD gil on 11/4/17.
 */
@Service
public class VideoConversion {

    @Value("${conversion.messaging.rabbitmq.conversion-queue}") public  String conversionQueue;
    @Value("${conversion.messaging.rabbitmq.conversion-exchange}") public  String conversionExchange;


  //  @Autowired RabbitTemplate rabbitTemplate;

    @Autowired VideoConversionRepository videoConversionRepository;

//    @Autowired
//    @Qualifier("video-conversion-template")
//    public void setRabbitTemplate(final RabbitTemplate template) {
//        this.rabbitTemplate = template;
//    }

    public void save(
                final ConversionRequest request,
                final ConversionResponse response) throws JsonProcessingException {

        final VideoConversions conversion = new VideoConversions(
                                                    response.getUuid().toString(),
                                                    request.getPath().toString(),
                                                    "GotS7E7_intro.mkv");
        Regions clientRegion = Regions.EU_CENTRAL_1;
        String bucketName = "edu.esipe.i3.ezipflix.frontend.lazare.fr";
        String filmkey="GotS7E7_intro.mkv";
        String fileName = "Game.of.Thrones.S07E07.1080p-intro.mkv";                     
        // Desactivation de mongoDB
        videoConversionRepository.save(conversion);
        // AWS S3
        AWSCredentials Acredentials = new BasicAWSCredentials(
        		  "AKIAISFOGJYQOSROMUKA", 
        		  "QJmEbQ+DPLga6jye5F3PUo0InkhRm4kPlUjytV1E" 
        		);
        
        AmazonS3 s3client = AmazonS3ClientBuilder
        		  .standard()
        		  .withCredentials(new AWSStaticCredentialsProvider(Acredentials))
        		  .withRegion(clientRegion)
        		  .build();
        // Upload a file as a new object with ContentType and title specified.
        PutObjectRequest S3request = new PutObjectRequest(bucketName, filmkey, new             File(fileName));
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType("plain/text");
        metadata.addUserMetadata("x-amz-meta-title", "video");
        S3request.setMetadata(metadata);
        s3client.putObject(S3request);
      

      
        //GOOGLE PUBSUB
        //final Message message = new Message(conversion.toJson().getBytes(), new MessageProperties());
        final String message = conversion.toJson();
        String jsonPath = "google_credentials.json";
        GoogleCredentials Gcredentials = null;
		try {
			Gcredentials = GoogleCredentials.fromStream(new FileInputStream(jsonPath));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Publisher publisher = null;
        
      try {
      // Create a publisher instance with default settings bound to the topic
      ProjectTopicName topicName = ProjectTopicName.of("edu-esipe-i3-ezipflix-lazare", "topic1");
      publisher = Publisher.newBuilder(topicName).setCredentialsProvider(FixedCredentialsProvider.create(Gcredentials))      .build();
      // convert message to bytes
      ByteString data = ByteString.copyFromUtf8(message);
      PubsubMessage pubsubMessage = PubsubMessage.newBuilder()
        .setData(data)
        .build();
      // Schedule a message to be published. Messages are automatically batched.
      publisher.publish(pubsubMessage);
    } catch (IOException e) {
		e.printStackTrace();
	} finally {
      if (publisher != null) {
        // When finished with the publisher, shutdown to free up resources.
        try {
			publisher.shutdown();
		} catch (Exception e) {
			e.printStackTrace();
		}
      }
    }

      //  rabbitTemplate.convertAndSend(conversionExchange, conversionQueue,  conversion.toJson());
    }

}
