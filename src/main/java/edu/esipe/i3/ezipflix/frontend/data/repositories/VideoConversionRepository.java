package edu.esipe.i3.ezipflix.frontend.data.repositories;

import edu.esipe.i3.ezipflix.frontend.data.entities.VideoConversions;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
//import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

import java.util.UUID;

/**
 * Created by Gilles GIRAUD gil on 11/4/17.
 */
@Repository
public class VideoConversionRepository {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    public void save(VideoConversions conversions) {
        dynamoDBMapper.save(conversions);
    }
}
